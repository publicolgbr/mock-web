FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /usr/app
COPY target/*.jar .
EXPOSE 8082
ENTRYPOINT ["java","-jar","mock-web-0.0.1-SNAPSHOT.jar"]