package com.qvision.mockweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockWebApplication.class, args);
    }

}
