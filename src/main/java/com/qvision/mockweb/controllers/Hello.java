package com.qvision.mockweb.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/greet")
public class Hello {
    @GetMapping(produces = "application/json")
    public ResponseEntity<String> getGreet() {
        return new ResponseEntity<>(buildGreet(), HttpStatus.OK);
    }

    private String buildGreet() {
        return "{" +
                "\"hello\":\"Q-Vision\"" +
                " }";
    }
}
